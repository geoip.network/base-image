FROM python:3.10-alpine3.16

RUN apk add --no-cache "build-base" "musl-dev" "libffi-dev" "boost-dev>1.77.0" "boost-build>1.77.0" "boost-static>1.77.0" "openssl-dev>1.1.1" "openssl-libs-static>1.1.1" "zlib-dev>1.2.12" "zlib-static>1.2.12" "rust>1.60" "cargo" "libmagic" "file-dev" "imagemagick" "imagemagick-libs" "imagemagick-dev"
RUN pip install --upgrade pip
RUN pip install poetry
ADD ./poetry.lock /base/poetry.lock
ADD ./pyproject.toml /base/pyproject.toml
RUN cd base; poetry config virtualenvs.create false; poetry install
